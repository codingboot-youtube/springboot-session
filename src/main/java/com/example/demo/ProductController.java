package com.example.demo;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.exceptions.ProductNotFoundException;

@Controller
@ResponseBody
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

	@GetMapping("/products")
	public ServiceResponse<List<Product>> getAll() {

		return new ServiceResponse<List<Product>>("00", "SUCCESS", productRepository.findAll());

	}

	@GetMapping("/products/{id}")
	public ServiceResponse<Product> getProduct(@PathVariable Integer id) {
		Optional<Product> optional = productRepository.findById(id);
		
		if(optional.isPresent()) {
			return new ServiceResponse<Product>("00", "SUCCESS",optional.get() );
		}else {
			throw new ProductNotFoundException("Invalid product id");
		}
	}
	
	@PostMapping("/products")
	public ServiceResponse<Product> createProduct(@RequestBody Product product) {
		return new ServiceResponse<Product>("00", "SUCCESS", productRepository.save(product));

	}

}
